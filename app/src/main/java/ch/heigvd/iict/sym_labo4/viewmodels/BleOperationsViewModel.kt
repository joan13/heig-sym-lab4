package ch.heigvd.iict.sym_labo4.viewmodels

import android.app.Application
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattCharacteristic.*
import android.bluetooth.BluetoothGattService
import android.content.Context
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import no.nordicsemi.android.ble.BleManager
import no.nordicsemi.android.ble.data.Data
import no.nordicsemi.android.ble.observer.ConnectionObserver
import java.util.*
import java.text.SimpleDateFormat


/**
 * Project: Labo4
 * Created by fabien.dutoit on 11.05.2019
 * Updated by fabien.dutoit on 18.10.2021
 * (C) 2019 - HEIG-VD, IICT
 */
class BleOperationsViewModel(application: Application) : AndroidViewModel(application) {

    private var ble = SYMBleManager(application.applicationContext)
    private var mConnection: BluetoothGatt? = null

    //live data - observer
    val isConnected = MutableLiveData(false)

    @RequiresApi(Build.VERSION_CODES.N)
    val time = MutableLiveData<Optional<Calendar>>()
    val temp = MutableLiveData<Optional<Double>>()
    val nClicks = MutableLiveData(0)

    private var nInitialClicks = 0

    //Services and Characteristics of the SYM Pixl
    private var timeService: BluetoothGattService? = null
    private var symService: BluetoothGattService? = null
    private var currentTimeChar: BluetoothGattCharacteristic? = null
    private var integerChar: BluetoothGattCharacteristic? = null
    private var temperatureChar: BluetoothGattCharacteristic? = null
    private var buttonClickChar: BluetoothGattCharacteristic? = null

    override fun onCleared() {
        super.onCleared()
        Log.d(TAG, "onCleared")
        ble.disconnect()
    }

    fun connect(device: BluetoothDevice) {
        Log.d(TAG, "User request connection to: $device")
        if (!isConnected.value!!) {
            ble.connect(device)
                    .retry(1, 100)
                    .useAutoConnect(false)
                    .enqueue()
        }
    }

    fun disconnect() {
        Log.d(TAG, "User request disconnection")
        ble.disconnect()
        mConnection?.disconnect()
    }

    /* TODO
        vous pouvez placer ici les différentes méthodes permettant à l'utilisateur
        d'interagir avec le périphérique depuis l'activité
     */

    @RequiresApi(Build.VERSION_CODES.N)
    fun readTemperature(): Boolean {
        if (!isConnected.value!! || temperatureChar == null)
            return false
        else
            return ble.readTemperature()
    }

    fun sendValue(value: Byte) {
        return ble.sendValue(value)
    }

    fun setTime(time: Calendar) {
        return ble.setTime(time, true)
    }

    private val bleConnectionObserver: ConnectionObserver = object : ConnectionObserver {
        override fun onDeviceConnecting(device: BluetoothDevice) {
            Log.d(TAG, "onDeviceConnecting")
            isConnected.value = false
        }

        override fun onDeviceConnected(device: BluetoothDevice) {
            Log.d(TAG, "onDeviceConnected")
            isConnected.value = true
        }

        override fun onDeviceDisconnecting(device: BluetoothDevice) {
            Log.d(TAG, "onDeviceDisconnecting")
            isConnected.value = false
        }

        override fun onDeviceReady(device: BluetoothDevice) {
            Log.d(TAG, "onDeviceReady")
        }

        override fun onDeviceFailedToConnect(device: BluetoothDevice, reason: Int) {
            Log.d(TAG, "onDeviceFailedToConnect")
        }

        override fun onDeviceDisconnected(device: BluetoothDevice, reason: Int) {
            if (reason == ConnectionObserver.REASON_NOT_SUPPORTED) {
                Log.d(TAG, "onDeviceDisconnected - not supported")
                Toast.makeText(getApplication(), "Device not supported - implement method isRequiredServiceSupported()", Toast.LENGTH_LONG).show()
            } else
                Log.d(TAG, "onDeviceDisconnected")
            isConnected.value = false
        }

    }

    private inner class SYMBleManager(applicationContext: Context) : BleManager(applicationContext) {
        /**
         * BluetoothGatt callbacks object.
         */
        private var mGattCallback: BleManagerGattCallback? = null

        public override fun getGattCallback(): BleManagerGattCallback {
            //we initiate the mGattCallback on first call, singleton
            if (mGattCallback == null) {
                mGattCallback = object : BleManagerGattCallback() {

                    public override fun isRequiredServiceSupported(gatt: BluetoothGatt): Boolean {
                        mConnection = gatt //trick to force disconnection

                        val foundSymService = gatt.services
                                .find { s -> s.uuid == UUID.fromString("3c0a1000-281d-4b48-b2a7-f15579a1c38f") }
                                ?: return false

                        val graphChar = foundSymService.characteristics.find { c -> c.uuid == UUID.fromString("3c0a1001-281d-4b48-b2a7-f15579a1c38f") }
                                ?: return false

                        if (graphChar.properties and PROPERTY_WRITE == 0) {
                            Log.i(TAG, "Refused device due not being able to write graph")
                            return false;
                        }

                        val thermChar = foundSymService.characteristics.find { s -> s.uuid == UUID.fromString("3c0a1002-281d-4b48-b2a7-f15579a1c38f") }
                                ?: return false

                        if (thermChar.properties and PROPERTY_READ == 0) {
                            Log.i(TAG, "Refused device due not being able to read thermometer")
                            return false;
                        }

                        val buttonChar = foundSymService.characteristics.find { s -> s.uuid == UUID.fromString("3c0a1003-281d-4b48-b2a7-f15579a1c38f") }
                                ?: return false

                        if (buttonChar.properties and PROPERTY_NOTIFY == 0) {
                            Log.i(TAG, "Refused device due missing capability of buttons to notify")
                            return false;
                        }

                        val foundTimeService = gatt.services
                                .find { s -> s.uuid == UUID.fromString("00001805-0000-1000-8000-00805f9b34fb") }
                                ?: return false

                        val timeChar = foundTimeService.characteristics.find { s -> s.uuid == UUID.fromString("00002a2b-0000-1000-8000-00805f9b34fb") }
                                ?: return false

                        if (timeChar.properties and PROPERTY_WRITE == 0) {
                            Log.i(TAG, "Refused device due not being able to set time")
                            return false;
                        }

                        if (timeChar.properties and PROPERTY_NOTIFY == 0) {
                            Log.i(TAG, "Refused device due not being able to receive time updates")
                            return false;
                        }

                        symService = foundSymService
                        integerChar = graphChar
                        temperatureChar = thermChar
                        buttonClickChar = buttonChar

                        timeService = foundTimeService
                        currentTimeChar = timeChar

                        return true
                    }

                    @RequiresApi(Build.VERSION_CODES.N)
                    override fun initialize() {
                        setNotificationCallback(currentTimeChar).with { _, data ->
                            // A better return type would be 'Calendar?' but writing this without
                            // monadic composition desugaring is utterly unreadable.
                            fun parseBleCurrentTimeCharBytes(data: Data): Calendar {
                                val year: Int = data.getIntValue(Data.FORMAT_UINT16, 0)!!
                                val month: Int = data.getByte(2)!!.toInt()
                                val dom: Int = data.getByte(3)!!.toInt()
                                val hour: Int = data.getByte(4)!!.toInt()
                                val minute: Int = data.getByte(5)!!.toInt()
                                val second: Int = data.getByte(6)!!.toInt()
                                val dow: Int = data.getByte(7)!!.toInt()
                                val ms: Int = data.getByte(8)!! * 1000 / 256
                                // Reason is not used.

                                // For some reason, GregorianCalendar ends up adding one to the year...
                                val ret = GregorianCalendar(year - 1, month, dom, hour, minute, second)
                                // The gregorian calendar starts on monday.
                                ret.set(Calendar.DAY_OF_WEEK, (dow - 1) % 7)
                                ret.set(Calendar.MILLISECOND, ms)
                                return ret
                            }

                            val deviceTime = parseBleCurrentTimeCharBytes(data)
                            val format = SimpleDateFormat("E yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault())
                            val currentTimeStr = format.format(deviceTime.time)
                            Log.d(TAG, "Received time notification: $currentTimeStr")
                            time.value = Optional.of(deviceTime)
                        }
                        enableNotifications(currentTimeChar).enqueue()

                        setTime(Calendar.getInstance(), false) // Set current time in the device.
                        nInitialClicks = 0

                        readCharacteristic(buttonClickChar).with { _, deviceClicksOffset ->
                            nInitialClicks = deviceClicksOffset.getByte(0)!!.toInt()
                            setNotificationCallback(buttonClickChar).with { _, data ->
                                val deviceNClicks = data.getByte(0)!!
                                val newNClicks = deviceNClicks - nInitialClicks
                                Log.i(TAG, "Received button press. Total presses: $newNClicks")
                                nClicks.value = newNClicks  // Atomic update
                            }
                            enableNotifications(buttonClickChar).enqueue()
                        }.enqueue()
                    }

                    override fun onServicesInvalidated() {
                        //we reset services and characteristics
                        timeService = null
                        currentTimeChar = null
                        symService = null
                        integerChar = null
                        temperatureChar = null
                        buttonClickChar = null
                    }
                }
            }
            return mGattCallback!!
        }

        @RequiresApi(Build.VERSION_CODES.N)
        fun readTemperature(): Boolean {
            if (temperatureChar == null)
                return false
            readCharacteristic(temperatureChar).with { _, data -> temp.value = Optional.of(data.getIntValue(Data.FORMAT_UINT8, 0)!! / 10.0) }.enqueue()
            return true
        }

        /**
         * Sets the time on the device. If the time was not provided by the user, it is considered
         * to have an external time reference and not manually provided.
         * In either case, time zone changes nor daytime savings are considered nor propagated.
         */
        fun setTime(time: Calendar, userProvided: Boolean) {
            fun toBleCurrentTimeCharBytes(time: Calendar): Data {
                val year = time.get(Calendar.YEAR)
                val y0: Byte = (year % 256).toByte()
                val y1: Byte = (year / 256).toByte()
                val m2: Byte = time.get(Calendar.MONTH).toByte()
                val d3: Byte = time.get(Calendar.DAY_OF_MONTH).toByte()
                val h4: Byte = time.get(Calendar.HOUR_OF_DAY).toByte()
                val m5: Byte = time.get(Calendar.MINUTE).toByte()
                val s6: Byte = time.get(Calendar.SECOND).toByte()
                val wd7: Byte = time.get(Calendar.DAY_OF_WEEK).toByte()
                val f8: Byte = (time.get(Calendar.MILLISECOND) * 256 / 1000).toByte()
                val r9: Byte = (if (userProvided) 3 else 0).toByte()
                return Data(byteArrayOf(y0, y1, m2, d3, h4, m5, s6, wd7, f8, r9))
            }
            writeCharacteristic(currentTimeChar, toBleCurrentTimeCharBytes(time), WRITE_TYPE_DEFAULT).enqueue()
        }

        fun sendValue(value: Byte) {
            writeCharacteristic(integerChar, Data(byteArrayOf(value)), WRITE_TYPE_DEFAULT).enqueue()
        }
    }

    companion object {
        private val TAG = BleOperationsViewModel::class.java.simpleName
    }

    init {
        ble.setConnectionObserver(bleConnectionObserver)
    }

}
