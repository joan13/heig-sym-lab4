package ch.heigvd.iict.sym_labo4

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.opengl.GLSurfaceView
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import ch.heigvd.iict.sym_labo4.gl.OpenGLRenderer
import android.hardware.SensorManager
import android.util.Log


/**
 * Project: Labo4
 * Created by fabien.dutoit on 21.11.2016
 * Updated by fabien.dutoit on 06.11.2020
 * (C) 2016 - HEIG-VD, IICT
 */
class CompassActivity : AppCompatActivity(), SensorEventListener {

    //opengl
    private lateinit var opglr: OpenGLRenderer
    private lateinit var m3DView: GLSurfaceView

    //sensors
    private var mSensorManager: SensorManager? = null
    private var mAccelerometer: Sensor? = null
    private var mMagnetometer: Sensor? = null

    //sensor matrices
    private var mGravity = FloatArray(3)
    private var mMagnetic = FloatArray(3)

    //calculated matrix
    private var r = FloatArray(16)

    //opengl
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // we need fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        // we initiate the view
        setContentView(R.layout.activity_compass)

        //we create the renderer
        opglr = OpenGLRenderer(applicationContext)

        // link to GUI
        m3DView = findViewById(R.id.compass_opengl)

        //init opengl surface view
        m3DView.setRenderer(opglr)

        //init sensor variables
        mSensorManager = getSystemService(SENSOR_SERVICE) as? SensorManager
        Log.i("CompassActivity", "system service assigned")
        mAccelerometer = mSensorManager?.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        Log.i("CompassActivity", "accelerometer found")
        mMagnetometer  = mSensorManager?.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)
        Log.i("CompassActivity", "magnetometer found")


    }

    override fun onResume() {
        super.onResume()
        mSensorManager?.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST)
        mSensorManager?.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_FASTEST)
        Log.i("CompassActivity", "listeners registered")
    }

    override fun onPause() {
        super.onPause()
        mSensorManager?.unregisterListener(this, mAccelerometer)
        mSensorManager?.unregisterListener(this, mMagnetometer)
        Log.i("CompassActivity", "listeners unregistered")
    }

    //sensors
    override fun onSensorChanged(event: SensorEvent) {
        var sensor: Sensor? = event.sensor
        var success: Boolean
        if (sensor?.type == Sensor.TYPE_ACCELEROMETER) {
            mGravity = event.values
        }
        else if (sensor?.type == Sensor.TYPE_MAGNETIC_FIELD) {
            mMagnetic = event.values
        }
        success = SensorManager.getRotationMatrix(r, null, mGravity, mMagnetic)

        if (success)
            r = opglr.swapRotMatrix(r)
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) { /*do nothing */
    }

}