package ch.heigvd.iict.sym_labo4

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import ch.heigvd.iict.sym_labo4.abstractactivies.BaseTemplateActivity
import android.bluetooth.BluetoothAdapter
import ch.heigvd.iict.sym_labo4.viewmodels.BleOperationsViewModel
import ch.heigvd.iict.sym_labo4.adapters.ResultsAdapter
import android.bluetooth.BluetoothManager
import android.bluetooth.le.*
import android.os.*
import android.text.format.DateFormat
import androidx.lifecycle.ViewModelProvider
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.DialogFragment
import java.lang.NumberFormatException
import java.text.SimpleDateFormat
import java.util.*
import java.util.function.BiConsumer
import java.util.function.Consumer


/**
 * Project: Labo4
 * Created by fabien.dutoit on 11.05.2019
 * Updated by fabien.dutoit on 06.11.2020
 * (C) 2019 - HEIG-VD, IICT
 */
class BleActivity : BaseTemplateActivity() {
    //system services
    private lateinit var bluetoothAdapter: BluetoothAdapter

    //view model
    private lateinit var bleViewModel: BleOperationsViewModel

    //gui elements
    private lateinit var operationPanel: View
    private lateinit var scanPanel: View
    private lateinit var scanResults: ListView
    private lateinit var emptyScanResults: TextView
    private lateinit var tempTV: TextView
    private lateinit var tempUpdateB: Button
    private lateinit var buttonPressesTV: TextView
    private lateinit var valueInput: EditText
    private lateinit var sendValueB: Button
    private lateinit var timeTV: TextView
    private lateinit var sendDateB: Button

    //menu elements
    private var scanMenuBtn: MenuItem? = null
    private var disconnectMenuBtn: MenuItem? = null

    //adapters
    private lateinit var scanResultsAdapter: ResultsAdapter

    //states
    private var handler = Handler(Looper.getMainLooper())

    private var isScanning = false

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ble)

        //enable and start bluetooth - initialize bluetooth adapter
        val bluetoothManager = getSystemService(BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = bluetoothManager.adapter

        //link GUI
        operationPanel = findViewById(R.id.ble_operation)
        scanPanel = findViewById(R.id.ble_scan)
        scanResults = findViewById(R.id.ble_scanresults)
        emptyScanResults = findViewById(R.id.ble_scanresults_empty)
        tempTV = findViewById(R.id.temperature)
        tempUpdateB = findViewById(R.id.tempUpdateButton)
        buttonPressesTV = findViewById(R.id.buttonpresses)
        valueInput = findViewById(R.id.valueInput)
        sendValueB = findViewById(R.id.sendValueButton)
        timeTV = findViewById(R.id.time)
        sendDateB = findViewById(R.id.sendDateButton)

        //manage scanned item
        scanResultsAdapter = ResultsAdapter(this)
        scanResults.adapter = scanResultsAdapter
        scanResults.emptyView = emptyScanResults

        //connect to view model
        bleViewModel = ViewModelProvider(this).get(BleOperationsViewModel::class.java)

        updateGui()

        //events
        scanResults.setOnItemClickListener { _: AdapterView<*>?, _: View?, position: Int, _: Long ->
            runOnUiThread {
                //we stop scanning
                scanLeDevice(false)
                //we connect
                bleViewModel.connect(scanResultsAdapter.getItem(position).device)
            }
        }

        //ble events
        bleViewModel.isConnected.observe(this, { updateGui() })
        bleViewModel.temp.observe(this, {
            it.ifPresent { v ->
                tempTV.text = v.toString()
            }
            if (!it.isPresent) {
                tempTV.text = "Retrieving temperature failed, please try again."
            }
            tempUpdateB.visibility = View.VISIBLE
        })
        tempUpdateB.setOnClickListener { _ ->
            tempUpdateB.visibility = View.INVISIBLE
            tempTV.text = "Updating..."
            bleViewModel.readTemperature()
        }
        bleViewModel.nClicks.observe(this, {
            buttonPressesTV.text = it.toString()
        })
        sendValueB.setOnClickListener { _ ->
            try {
                bleViewModel.sendValue(valueInput.text.toString().toByte())
            } catch (nfe: NumberFormatException) {
                Toast.makeText(this, "The specified value cannot be sent as a single byte.", Toast.LENGTH_SHORT).show()
            }
        }
        bleViewModel.time.observe(this, {
            it.ifPresent { v ->
                val format = SimpleDateFormat("E yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault())
                timeTV.text = format.format(v.time)
            }
        })
        sendDateB.setOnClickListener {
            DateTimePickerDialog{ c: Calendar ->
                val format = SimpleDateFormat("E yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault())
                Log.i(TAG, "Setting time to ${format.format(c.time)}")
                bleViewModel.setTime(c)
            }.show(supportFragmentManager, "timePicker")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.ble_menu, menu)
        //we link the two menu items
        scanMenuBtn = menu.findItem(R.id.menu_ble_search)
        disconnectMenuBtn = menu.findItem(R.id.menu_ble_disconnect)
        //we update the gui
        updateGui()
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.menu_ble_search) {
            if (isScanning) scanLeDevice(false) else scanLeDevice(true)
            return true
        } else if (id == R.id.menu_ble_disconnect) {
            bleViewModel.disconnect()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPause() {
        super.onPause()
        if (isScanning) scanLeDevice(false)
        if (isFinishing) bleViewModel.disconnect()
    }

    /*
     * Method used to update the GUI according to BLE status:
     * - connected: display operation panel (BLE control panel)
     * - not connected: display scan result list
     */
    private fun updateGui() {
        val isConnected = bleViewModel.isConnected.value
        if (isConnected != null && isConnected) {

            scanPanel.visibility = View.GONE
            operationPanel.visibility = View.VISIBLE

            if (scanMenuBtn != null && disconnectMenuBtn != null) {
                scanMenuBtn!!.isVisible = false
                disconnectMenuBtn!!.isVisible = true
            }
        } else {
            operationPanel.visibility = View.GONE
            scanPanel.visibility = View.VISIBLE

            if (scanMenuBtn != null && disconnectMenuBtn != null) {
                disconnectMenuBtn!!.isVisible = false
                scanMenuBtn!!.isVisible = true
            }
        }
    }

    //this method needs user grant localisation and/or bluetooth permissions, our demo app is requesting them on MainActivity
    private fun scanLeDevice(enable: Boolean) {
        val bluetoothScanner = bluetoothAdapter.bluetoothLeScanner

        if (enable) {
            //config
            val builderScanSettings = ScanSettings.Builder()
            builderScanSettings.setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
            builderScanSettings.setReportDelay(0)

            val serviceUuid = ParcelUuid.fromString("3c0a1000-281d-4b48-b2a7-f15579a1c38f")
            val scanFilter = ScanFilter.Builder().setServiceUuid(serviceUuid).build()

            //reset display
            scanResultsAdapter.clear()
            bluetoothScanner.startScan(arrayListOf(scanFilter), builderScanSettings.build(), leScanCallback)
            Log.d(TAG, "Start scanning...")
            isScanning = true

            //we scan only for 15 seconds
            handler.postDelayed({ scanLeDevice(false) }, 15 * 1000L)
        } else {
            bluetoothScanner.stopScan(leScanCallback)
            isScanning = false
            Log.d(TAG, "Stop scanning (manual)")
        }
    }

    // Device scan callback.
    private val leScanCallback: ScanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            super.onScanResult(callbackType, result)
            runOnUiThread { scanResultsAdapter.addDevice(result) }
        }
    }

    companion object {
        private val TAG = BleActivity::class.java.simpleName
    }

    // Class must be public static class to be properly recreated by the AppCompat manager.
    class PickTimeDialog(private val onResult: BiConsumer<Int, Int>) : DialogFragment(), TimePickerDialog.OnTimeSetListener {
        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            // Use the current time as the default values for the picker
            val c = Calendar.getInstance()
            val hour = c.get(Calendar.HOUR_OF_DAY)
            val minute = c.get(Calendar.MINUTE)

            return TimePickerDialog(activity, this, hour, minute, DateFormat.is24HourFormat(activity))
        }

        @RequiresApi(Build.VERSION_CODES.N)
        override fun onTimeSet(view: TimePicker?, hour: Int, min: Int) {
            onResult.accept(hour, min)
        }
    }

    // Class must be public static class to be properly recreated by the AppCompat manager.
    class DateTimePickerDialog(private val onResult : Consumer<Calendar>) : DialogFragment(), DatePickerDialog.OnDateSetListener {

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            return DatePickerDialog(requireActivity(), this, year, month, day)
        }

        @RequiresApi(Build.VERSION_CODES.N)
        override fun onDateSet(view: DatePicker?, year: Int, month: Int, day: Int) {
            PickTimeDialog{ hour, min ->
                onResult.accept(GregorianCalendar(year, month, day, hour, min))
            }.show(requireActivity().supportFragmentManager, "timePicker")
        }
    }
}
