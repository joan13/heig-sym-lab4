---
toc: yes
date: 3 Mar 2021
title: Systèmes Mobiles
subtitle: Laboratoire 4
author:
  - Roosemberth Palacios
  - Joan Maillard
---

# Introduction

The goal of this lab is to familiarize ourselves with the implementation of two concepts:
Communicating with a BLE device and using sensors.

# Answers to questions

## 1.2

The most likely explanation for the jitter the arrow shows is the fact that sensors' data oftentimes is also unstable.
This comes from a variety of reasons; for example, the variations in the magnetic field sensed by the magnetometer
inside the phone is affected by magnetic disturbances produced by the many rapidly-varying electrical currents that surround us.

This is all without mentioning the event-based nature of this implementation, which impairs measures like this by its asynchrounous
nature.

A good way to mitigate those issues would be to implement data smoothing by way of a [Kalman filter](https://en.wikipedia.org/wiki/Kalman_filter).

## 2.2.1

Since the scale of the sensor is well known, it may be simpler for the embedded
device to realize all operations using their integer representations.
Moreover, floating point calculations are oftentimes more power-hungry and less
efficient on commodity embedded systems.

## 2.2.2

The standard [battery service](https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/Gatt/Xml/Services/org.bluetooth.service.battery_service.xml) provides this information,
As described by this standard, this service contains (among others) a "battery level" characteristic with properties Read (mandatory) and Notify (optional).
From the standard informative text:

> The Battery Level characteristic is read using the GATT Read Characteristic Value sub-procedure and returns the current battery level as a percentage from 0% to 100%; 0% represents a battery that is fully discharged, 100% represents a battery that is fully charged.
