# Partenaires clés
 - Magasin de jardinerie
 - Communauté d'amateurs des plantes.

# Activités clés
 - Dév d'une plateforme pour les soins de plantes (app, site, communauté).
 - Capte à capteurs.
 
# Ressources clés
 - Application mobile.
 - Communauté.
 - Constructeurs des cartes à capteurs.

# Propositions de valeur
- Rappels a l'utilisateur les besoins de ses plantes.
- Conseils l'utilisateur sur les besoins de ses plantes.
- Carte capteurs pour un suivi détaillé.
 
# Relations avec les clients
 - Forum communautaire.
 - Support via nos parternaires (tier 1).

# Canaux
 - Distribution de l'application sur diverses app stores.
 - Vente de carte à capteurs sur les boutiques partenaires.
 - Plateforme en ligne.

# Segments de clientèle
 - Marché segmenté

# Structure des coûts
- Exploitation en ligne
- Maintenance et gestion de la communauté
- Développement {app, carte, site en ligne}
- Gestion des basses de données

# Flux de revenus
 - Vente de cartes à capteurs.
 - Partenariat avec magasins de jardinerie.

----------

# Segments de clientèle
 - 

# Canaux
 - Magasins Partenaire
 - Vente en ligne (pas directement mais redirige vers le site des magasins partenaires)


# Partenaires
- Fournisseurs (Ex: Fleuristes, Paysan => qui peuvent partager leurs connaissances pour créer une communauté fiable, Migros, Magasin de jardinage, ...)
- Base de données des plantes (les magasins enrichissent la base de données en fonction des plantes qu'ils vendent, pour encourager les client a les acheter et les membres de la communauté donnent des informations sur les plantes qu'ils aiment)
 
# Relation client
- Communauté

# Resources
- Les composantes electroniques i.e. capteurs, carte
- Resources informatiques i.e. base de données
- Resources humaines i.e. communauté























